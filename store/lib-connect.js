const r = require('rethinkdb');
const config = require('./config/database');
const response = require('../network/response');

const connect = async (req, res, next) => {
   await r.connect(config).then((conn) => {
       req._rdb = conn;
       next();
   }).catch((error) => {
       response.error(req, res, error, 500);
   })
}

const close = async (req, res, next) => {
    await req._rdb.close();
};

module.exports = {
    connect,
    close
}